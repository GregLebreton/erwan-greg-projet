# creation dossier workspace
sudo mkdir workspace
# update packages
sudo apt update -y
# install git
sudo apt install git -y
# install python
sudo apt install python -y
# install docker dependances
sudo apt-get install -y \
 apt-transport-https \
 ca-certificates \
 curl \
 gnupg \
 lsb-release
# install docker par un script auto
sudo curl -fsSL https://get.docker.com -o get-docker.sh
sudo chmod +x get-docker.sh
sudo sh get-docker.sh
